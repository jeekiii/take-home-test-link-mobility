package com.link.mobility.takehome.service;

import com.link.mobility.takehome.dto.PhoneNumbersDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

//Not really necessary to inject that, but it's how most test will work so no reason to break the pattern just because there is only one service
//Personally I'm a fan of whitebox testing and it might be what I'd go for (making private function package-private and testing them here.
//But some people are really scared of that, so for now I'll do blackbox testing
@ExtendWith(MockitoExtension.class)
public class PhoneNumberServiceTest {

    @InjectMocks
    private PhoneNumberService phoneNumberService;

    @Test
    void formatNumber_happyPath() {
        PhoneNumbersDto phoneNumbersDto = new PhoneNumbersDto(List.of("+46709234893","070-3306661","0046709234893","70543007"));
        assertThat(phoneNumberService.formatNumbers(phoneNumbersDto)).isEqualTo(
                new PhoneNumbersDto((List.of("+46709234893","+46703306661","+46709234893","invalid")))
        );
    }

    @Test
    void formatNumber_invalidNumbers() {
        PhoneNumbersDto phoneNumbersDto = new PhoneNumbersDto(List.of("333","0046709234893abcd","093//","705430077878"));
        assertThat(phoneNumberService.formatNumbers(phoneNumbersDto)).isEqualTo(
                new PhoneNumbersDto((List.of("invalid","invalid","invalid","invalid")))
        );
    }

    @Test
    void formatNumber_emptyArray() {
        PhoneNumbersDto phoneNumbersDto = new PhoneNumbersDto(Collections.emptyList());
        assertThat(phoneNumberService.formatNumbers(phoneNumbersDto)).isEqualTo(
                new PhoneNumbersDto(Collections.emptyList())
        );
    }
}