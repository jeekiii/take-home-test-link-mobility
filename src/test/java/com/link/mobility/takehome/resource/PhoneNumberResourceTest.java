package com.link.mobility.takehome.resource;

import com.link.mobility.takehome.dto.PhoneNumbersDto;
import com.link.mobility.takehome.service.PhoneNumberService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.verifyNoInteractions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class PhoneNumberResourceTest {
    private MockMvc mockMvc;

    @InjectMocks
    private PhoneNumberResource phoneNumberResource;

    @Mock
    private PhoneNumberService phoneNumberService;

    @BeforeEach
    public void init() {
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(phoneNumberResource)
                .build();
    }

    @Test
    public void formatNumbers_happyPath() throws Exception{
        PhoneNumbersDto input = new PhoneNumbersDto(List.of("+46709234893", "dd"));
        String serializedInput = "{ \"numbers\": [\"+46709234893\", \"dd\"]}";

        String serializedOutput = "{ \"numbers\": [\"+46709234893\", \"invalid\"]}";
        when(phoneNumberService.formatNumbers(input)).thenReturn(new PhoneNumbersDto(List.of("+46709234893", "invalid")));
        mockMvc.perform(get("/phoneNumber/format")
                .content(serializedInput)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(serializedOutput));
    }

    @Test
    public void formatNumbers_emptyInput() throws Exception{
        String serializedInput = "{}";

        mockMvc.perform(get("/phoneNumber/format")
                        .content(serializedInput)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        verifyNoInteractions(phoneNumberService);
    }
}
