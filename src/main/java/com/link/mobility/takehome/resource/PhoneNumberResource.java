package com.link.mobility.takehome.resource;
import com.link.mobility.takehome.dto.PhoneNumbersDto;
import com.link.mobility.takehome.service.PhoneNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(path="/phoneNumber")
public class PhoneNumberResource {
    @Autowired
    private PhoneNumberService phoneNumberService;

    @GetMapping("/format")
    public ResponseEntity<PhoneNumbersDto> formatPhoneNumbers(@RequestBody @Valid PhoneNumbersDto phoneNumbersDto) {
        return ResponseEntity.ok().body(phoneNumberService.formatNumbers(phoneNumbersDto));
    }
}
