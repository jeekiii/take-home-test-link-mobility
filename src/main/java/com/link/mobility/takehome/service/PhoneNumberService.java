package com.link.mobility.takehome.service;

import com.link.mobility.takehome.dto.PhoneNumbersDto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PhoneNumberService {
    //number of digits in phone number, without area code
    private static final int PHONE_NUMBER_DIGIT_COUNT = 9;
    private static final int AREA_CODE_DIGIT_COUNT = 2;
    private static final String SWEDISH_AREA_CODE = "46";
    public PhoneNumbersDto formatNumbers(PhoneNumbersDto phoneNumbersDto) {
        List<String> phoneNumberArray = phoneNumbersDto.getNumbers();
        List<String> resultingPhoneNumberArray = phoneNumberArray
                .stream()
                .map(this::formatPhoneNumber)
                .collect(Collectors.toList());

        return new PhoneNumbersDto(resultingPhoneNumberArray);
    }

    private String formatPhoneNumber(String phoneNumber) {
        if(containsUnexpectedCharacters(phoneNumber)) {
            return "invalid";
        }
        phoneNumber = removeUnnecessaryCharacters(phoneNumber);
        phoneNumber = addAreaCode(phoneNumber);
        //usually it's better to test at the start but here there are too many valid options and this works better
        if(!isValid(phoneNumber)) {
            return "invalid";
        }
        return phoneNumber;
    }

    //The specs does not specifies how to handle this. I might ask the Analyst/PO for their opinion on this
    private boolean containsUnexpectedCharacters(String phoneNumber) {
        return !phoneNumber.matches("[\\d./+\\- ]*");
    }

    private boolean isValid(String phoneNumber) {
        return phoneNumber.startsWith("+46") && phoneNumber.length() == 1 + AREA_CODE_DIGIT_COUNT + PHONE_NUMBER_DIGIT_COUNT;
    }

    private String addAreaCode(String phoneNumber) {
        if(isAreaCodeAndPhoneNumber(phoneNumber)) {
            return "+" + phoneNumber;
        } else if(startsWithTwoLeadingZeros(phoneNumber)) {
            return "+" + phoneNumber.substring(2);
        } else if(startsWithLeadingZero(phoneNumber)) {
            return "+" + SWEDISH_AREA_CODE + phoneNumber.substring(1);
        } else if(isOnlyPhoneNumber(phoneNumber)) {
            //Not in the spec but logical, that's how people use phone numbers in norway. Might ask analyst/PO
            return "+" + SWEDISH_AREA_CODE + phoneNumber;
        } else {
            return phoneNumber;
        }
    }

    private boolean isOnlyPhoneNumber(String phoneNumber) {
        return phoneNumber.length() == PHONE_NUMBER_DIGIT_COUNT;
    }

    private boolean startsWithLeadingZero(String phoneNumber) {
        return phoneNumber.startsWith("0") && phoneNumber.length() == 1 + PHONE_NUMBER_DIGIT_COUNT;
    }

    private boolean startsWithTwoLeadingZeros(String phoneNumber) {
        return phoneNumber.startsWith("00") && phoneNumber.length() == 2 + AREA_CODE_DIGIT_COUNT + PHONE_NUMBER_DIGIT_COUNT;
    }

    private boolean isAreaCodeAndPhoneNumber(String phoneNumber) {
        return phoneNumber.startsWith(SWEDISH_AREA_CODE) && phoneNumber.length() == AREA_CODE_DIGIT_COUNT + PHONE_NUMBER_DIGIT_COUNT;
    }

    private String removeUnnecessaryCharacters(String phoneNumber) {
        return phoneNumber.replaceAll("\\D", "");
    }
}
